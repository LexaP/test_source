### Building SPS-Stereo

1. Prerequisites
    * [libpng](http://www.libpng.org/pub/png/libpng.html)
    * [png++](http://www.nongnu.org/pngpp/)
    * CUDA(For GPU version only)
2. Building CPU Version
    1. type 'cmake .'
    2. type 'make'
3. Building GPU Version
    1. type './build.sh' or 'make'


**Outputs**  

* `000000_10_disp.png`  
Disparity image (PNG 16bit grayscale format)  
(Disparity value) = (Pixel value)/256.0

* `000000_10L_seg.png`  
Segmentation image (PNG 16bit grayscale format)  
(Segment ID) = (Pixel value)

* `000000_10L_plane.png`  
Estimated disparity planes  
the number of lines = the number of segments  
Each line includes parameters of disparity plane of a segment: `(A_i, B_i, C_i)`

* `000000_10L_label.txt`  
Boundary labeling result  
the number of lines = the number of boundaries  
Each line includes three entries: `SegmentID1 SegmentID2 boundary_label`  
`boundary_label`: 0 (Occlusion, SegmentID1 is front), 1 (Occlusion, SegmentID2 is front), 2 (Hinge), 3 (Coplanar)

* `000000_10L_boundary.png`  
Visualization of segmentation result  
Boundary color means a type of relationship between neighboring segments: Red/Blue-Occluion (Red side is front), Green-Hinge, Gray- Coplaner


### Running time comparison
Setting the maximum disparity to 128, GPU version is about 5 times faster than CPU version, as shown in the table below.

| **Image Size**    | **GPU Version**   | **CPU Version**   |
| :-:               | :-:               | :-:               |
| 640*480           | 0.12s             | 0.50s             |
| 450*435           | 0.06s             | 0.30s             |
| 433*381           | 0.06s             | 0.30s             |
